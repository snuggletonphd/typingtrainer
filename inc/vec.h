#ifndef _VEC_H_
#define _VEC_H_

#define VEC_INIT {NULL, 0, 0}

typedef struct Vector {
    char* c;
    int len;
    int cap;
} Vector;

void vec_push_c(Vector* v, char c);
void vec_push_str(Vector* v, const char* s, int len);
void vec_pop_c(Vector* v);
void vec_reset(Vector* v);
void vec_free(Vector* v);
int vec_is_equal(Vector* v1, Vector* v2);
int vec_str_equal(Vector* v, const char* s);

#endif
