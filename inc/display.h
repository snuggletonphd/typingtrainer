#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <ncurses.h>
#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "timer.h"

typedef struct display {
    int screenrows;
    int screencols;
    WINDOW* win;
    int posy;
    int posx;
    int winh;
    int winw;
    int offset;
} display;

display* display_init(int h, int w);

void display_create(display* d);

void display_recreate(display* d, int y, int x);

display* display_init_create(int h, int w);

display* display_init_create_kb(int h, int w, int offset);

void display_free(display* d);

void display_result(display* d, data* data, display* kbd, timer* t);

#endif
