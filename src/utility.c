#include "utility.h"


void die(const char* msg)
{
    perror(msg);
    curs_set(1);
    endwin();
    exit(1);
}

void quit(void)
{
    curs_set(1);
    endwin();
}

char* getlayout(int layout, int level, int uppercase)
{
    char* s = (char*)malloc(sizeof(char) * 23);
    int len = 0;
// COLEMAK {{{
    if (layout == COLEMAK) {
            if (level == 1) {
                len = 8;
                memcpy(s, "ARSTNEIO", len);
            } else if (level == 2) {
                len = 10;
                memcpy(s, "ARSTDHNEIO", len);
            } else if (level == 3) {
                len = 14;
                memcpy(s, "ARSTDHNEIOCVKM", len);
            } else if (level == 4) {
                len = 18;
                memcpy(s, "ARSTDHNEIOCVKMPGJL", len);
            } else if (level == 5) {
                len = 22;
                memcpy(s, "ARSTDHNEIOCVKMPGJLWFUY", len);
            } else {
                return NULL;
            }
// }}}
// COLEMAK_DH {{{
    } else if (layout == COLEMAK_DH) {
            if (level == 1) {
                len = 8;
                memcpy(s, "ARSTNEIO", len);
            } else if (level == 2) {
                len = 10;
                memcpy(s, "ARSTGMNEIO", len);
            } else if (level == 3) {
                len = 14;
                memcpy(s, "ARSTGMNEIODVKH", len);
            } else if (level == 4) {
                len = 18;
                memcpy(s, "ARSTGMNEIODVKHPBJL", len);
            } else if (level == 5) {
                len = 22;
                memcpy(s, "ARSTGMNEIODVKHPBJLWFUY", len);
            } else {
                return NULL;
            }
// }}}
// DVORAK {{{
    } else if (layout == DVORAK) {
            if (level == 1) {
                len = 8;
                memcpy(s, "AOEUHTNS", len);
            } else if (level == 2) {
                len = 10;
                memcpy(s, "AOEUIDHTNS", len);
            } else if (level == 3) {
                len = 14;
                memcpy(s, "AOEUJKIDBMHTNS", len);
            } else if (level == 4) {
                len = 18;
                memcpy(s, "AOEUIDHTNSJKBMPYFG", len);
            } else if (level == 5) {
                len = 22;
                memcpy(s, "AOEUIDHTNSJKBMPYFGCRLW", len);
            } else {
                return NULL;
            }
// }}}
// DVORAK_L {{{
    } else if (layout == DVORAK_L) {
            if (level == 1) {
                len = 6;
                memcpy(s, "CDTHEA", len);
            } else if (level == 2) {
                len = 9;
                memcpy(s, "CDTHEAWNI", len);
            } else if (level == 3) {
                len = 12;
                memcpy(s, "CDTHEAWNIRSO", len);
            } else if (level == 4) {
                len = 16;
                memcpy(s, "CDTHEAWNIRSOYUGV", len);
            } else if (level == 5) {
                len = 21;
                memcpy(s, "CDTHEAWNIRSOYUGVPFMLJ", len);
            } else {
                return NULL;
            }
// }}}
// DVORAK_R {{{
    } else if (layout == DVORAK_R) {
            if (level == 1) {
                len = 6;
                memcpy(s, "AEHTDC", len);
            } else if (level == 2) {
                len = 9;
                memcpy(s, "AEHTDCINW", len);
            } else if (level == 3) {
                len = 12;
                memcpy(s, "AEHTDCINWORS", len);
            } else if (level == 4) {
                len = 16;
                memcpy(s, "AEHTDCINWORSUYVG", len);
            } else if (level == 5) {
                len = 21;
                memcpy(s, "AEHTDCINWORSUYVGJLMFP", len);
            } else {
                return NULL;
            }
    } else {

        s = (char*)realloc(s, 27);
        len = 26;
        memcpy(s, "QWERTYUIOPASDFGHJKLZXCVBNM", len);

    }

    s[len] = '\0';
    if (!uppercase) {
        for (int i = 0; i < len; i++) {
            s[i] += 32;
        }
    }
    return s;
}
