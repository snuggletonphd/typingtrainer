#include "layoutconverter.h"

// It ain't pretty, but it does the job!

// Dvorak {{{

int convert_dvorak(int c)
{
    switch (c) {

// Number row {{{

        // 1-0 remain unchanged.

        case '-': return '[';
        case '_': return '{';

        case '=': return ']';
        case '+': return '}';

// }}}

// Top row {{{

        case 'q': return '\'';
        case 'Q': return '"';

        case 'w': return ',';
        case 'W': return '<';

        case 'e': return '.';
        case 'E': return '>';

        case 'r': return 'p';
        case 'R': return 'P';

        case 't': return 'y';
        case 'T': return 'Y';

        case 'y': return 'f';
        case 'Y': return 'F';

        case 'u': return 'g';
        case 'U': return 'G';

        case 'i': return 'c';
        case 'I': return 'C';

        case 'o': return 'r';
        case 'O': return 'R';

        case 'p': return 'l';
        case 'P': return 'L';

        case '[': return '/';
        case '{': return '?';

        case ']': return '=';
        case '}': return '+';

// }}}

// Home row {{{

        // A remains unchanged.

        case 's': return 'o';
        case 'S': return 'O';

        case 'd': return 'e';
        case 'D': return 'E';

        case 'f': return 'u';
        case 'F': return 'U';

        case 'g': return 'i';
        case 'G': return 'I';

        case 'h': return 'd';
        case 'H': return 'D';

        case 'j': return 'h';
        case 'J': return 'H';

        case 'k': return 't';
        case 'K': return 'T';

        case 'l': return 'n';
        case 'L': return 'N';

        case ';': return 's';
        case ':': return 'S';

        case '\'': return '-';
        case '"': return '_';

// }}}

// Bottom row {{{

        case 'z': return ';';
        case 'Z': return ':';

        case 'x': return 'q';
        case 'X': return 'Q';

        case 'c': return 'j';
        case 'C': return 'J';

        case 'v': return 'k';
        case 'V': return 'K';

        case 'b': return 'x';
        case 'B': return 'X';

        case 'n': return 'b';
        case 'N': return 'B';

        case 'm': return 'm';
        case 'M': return 'M';

        case ',': return 'w';
        case '<': return 'W';

        case '.': return 'v';
        case '>': return 'V';

        case '/': return 'z';
        case '?': return 'Z';

// }}}

        default: return c;
    }

}

// }}}

// Dvorak left-handed {{{

int convert_dvorak_l(int c)
{
    switch (c) {

// Number row {{{

//      []/pfmlj4321

        case '1': return '[';
        case '!': return '{';

        case '2': return ']';
        case '@': return '}';

        case '3': return '/';
        case '#': return '?';

        case '4': return 'p';
        case '$': return 'P';

        case '5': return 'f';
        case '%': return 'F';

        case '6': return 'm';
        case '^': return 'M';

        case '7': return 'l';
        case '&': return 'L';

        case '8': return 'j';
        case '*': return 'J';

        case '9': return '4';
        case '(': return '$';

        case '0': return '3';
        case ')': return '#';

        case '-': return '2';
        case '_': return '@';

        case '=': return '1';
        case '+': return '!';

// }}}

// Top row {{{

//      ;qbyurso.65=

        case 'q': return ';';
        case 'Q': return ':';

        case 'w': return 'q';
        case 'W': return 'Q';

        case 'e': return 'b';
        case 'E': return 'B';

        case 'r': return 'y';
        case 'R': return 'Y';

        case 't': return 'u';
        case 'T': return 'U';

        case 'y': return 'r';
        case 'Y': return 'R';

        case 'u': return 's';
        case 'U': return 'S';

        case 'i': return 'o';
        case 'I': return 'O';

        case 'o': return '.';
        case 'O': return '>';

        case 'p': return '6';
        case 'P': return '^';

        case '[': return '5';
        case '{': return '%';

        case ']': return '=';
        case '}': return '+';
// }}}

// Home row {{{

//      -kcdtheaz87

        case 'a': return '-';
        case 'A': return '_';

        case 's': return 'k';
        case 'S': return 'K';

        case 'd': return 'c';
        case 'D': return 'C';

        case 'f': return 'd';
        case 'F': return 'D';

        case 'g': return 't';
        case 'G': return 'T';

        case 'h': return 'h';
        case 'H': return 'H';

        case 'j': return 'e';
        case 'J': return 'E';

        case 'k': return 'a';
        case 'K': return 'A';

        case 'l': return 'z';
        case 'L': return 'Z';

        case ';': return '8';
        case ':': return '*';

        case '\'': return '7';
        case '\"': return '&';

// }}}

// Bottom row {{{

//      'xgvwni,09

        case 'z': return '\'';
        case 'Z': return '"';

        case 'x': return 'x';
        case 'X': return 'X';

        case 'c': return 'g';
        case 'C': return 'G';

        case 'v': return 'v';
        case 'V': return 'V';

        case 'b': return 'w';
        case 'B': return 'W';

        case 'n': return 'n';
        case 'N': return 'N';

        case 'm': return 'i';
        case 'M': return 'I';

        case ',': return ',';
        case '<': return '<';

        case '.': return '0';
        case '>': return ')';

        case '/': return '9';
        case '?': return '(';
// }}}

        default: return c;
    }
}

// }}}

// Dvorak right-handed {{{

int convert_dvorak_r(int c) {
    switch (c) {

// Number row {{{

        // 1-4 remains unchanged.

        case '5': return 'j';
        case '%': return 'J';

        case '6': return 'l';
        case '^': return 'L';

        case '7': return 'm';
        case '&': return 'M';

        case '8': return 'f';
        case '*': return 'F';

        case '9': return 'p';
        case '(': return 'P';

        case '0': return '/';
        case ')': return '?';

        case '-': return '[';
        case '_': return '{';

        case '=': return ']';
        case '+': return '}';
// }}}

// Top row {{{

        case 'q': return '5';
        case 'Q': return '%';

        case 'w': return '6';
        case 'W': return '^';

        case 'e': return 'q';
        case 'E': return 'Q';

        case 'r': return '.';
        case 'R': return '>';

        case 't': return 'o';
        case 'T': return 'O';

        case 'y': return 'r';
        case 'Y': return 'R';

        case 'u': return 's';
        case 'U': return 'S';

        case 'i': return 'u';
        case 'I': return 'U';

        case 'o': return 'y';
        case 'O': return 'Y';

        case 'p': return 'b';
        case 'P': return 'B';

        case '[': return ';';
        case '{': return ':';

        case ']': return '=';
        case '}': return '+';
// }}}

// Home row {{{

        case 'a': return '7';
        case 'A': return '&';

        case 's': return '8';
        case 'S': return '*';

        case 'd': return 'z';
        case 'D': return 'Z';

        case 'f': return 'a';
        case 'F': return 'A';

        case 'g': return 'e';
        case 'G': return 'E';

        case 'h': return 'h';
        case 'H': return 'H';

        case 'j': return 't';
        case 'J': return 'T';

        case 'k': return 'd';
        case 'K': return 'D';

        case 'l': return 'c';
        case 'L': return 'C';

        case ';': return 'k';
        case ':': return 'K';

        case '\'': return '-';
        case '"': return '_';

// }}}

// Bottom row {{{

        case 'z': return '9';
        case 'Z': return '(';

        case 'x': return '0';
        case 'X': return ')';

        case 'c': return 'x';
        case 'C': return 'X';

        case 'v': return ',';
        case 'V': return '<';

        case 'b': return 'i';
        case 'B': return 'I';

        // N remains unchanged.

        case 'm': return 'w';
        case 'M': return 'W';

        case ',': return 'v';
        case '<': return 'V';

        case '.': return 'g';
        case '>': return 'G';

        case '/': return '\'';
        case '?': return '"';
// }}}

        default: return c;
    }
}

// }}}

// Colemak {{{

int convert_colemak(int c)
{
    switch (c) {

    // The entire number row remains unchanged.

// Top row {{{

        // Q, W remain unchanged.

        case 'e': return 'f';
        case 'E': return 'F';

        case 'r': return 'p';
        case 'R': return 'P';

        case 't': return 'g';
        case 'T': return 'G';

        case 'y': return 'j';
        case 'Y': return 'J';

        case 'u': return 'l';
        case 'U': return 'L';

        case 'i': return 'u';
        case 'I': return 'U';

        case 'o': return 'y';
        case 'O': return 'Y';

        case 'p': return ';';
        case 'P': return ':';

        // [, ] remain unchanged.

// }}}

// Home row {{{

        // A remain unchanged.

        case 's': return 'r';
        case 'S': return 'R';

        case 'd': return 's';
        case 'D': return 'S';

        case 'f': return 't';
        case 'F': return 'T';

        case 'g': return 'd';
        case 'G': return 'D';

        // H remain unchanged.

        case 'j': return 'n';
        case 'J': return 'N';

        case 'k': return 'e';
        case 'K': return 'E';

        case 'l': return 'i';
        case 'L': return 'I';

        case ';': return 'o';
        case ':': return 'O';

        // ', " remain unchanged.

// }}}

// Bottom row {{{

        // The entire row, save from N is unaltered.

        case 'n': return 'k';
        case 'N': return 'K';

// }}}

        default: return c;
    }

}

// }}}

// Colemak-dh {{{

int convert_colemak_dh(int c)
{
    switch (c) {

    // The entire number row is unchanged.

// Top row {{{

        // Q and W remain unchanged.

        case 'e': return 'f';
        case 'E': return 'F';

        case 'r': return 'p';
        case 'R': return 'P';

        case 't': return 'b';
        case 'T': return 'B';

        case 'y': return 'j';
        case 'Y': return 'J';

        case 'u': return 'l';
        case 'U': return 'L';

        case 'i': return 'u';
        case 'I': return 'U';

        case 'o': return 'y';
        case 'O': return 'Y';

        case 'p': return ';';
        case 'P': return ':';

        // [ and ] are unchanged.

// }}}

// Home row {{{

        // A is unchanged.

        case 's': return 'r';
        case 'S': return 'R';

        case 'd': return 's';
        case 'D': return 'S';

        case 'f': return 't';
        case 'F': return 'T';

        // G is unchanged.

        case 'h': return 'm';
        case 'H': return 'M';

        case 'j': return 'n';
        case 'J': return 'N';

        case 'k': return 'e';
        case 'K': return 'E';

        case 'l': return 'i';
        case 'L': return 'I';

        case ';': return 'o';
        case ':': return 'O';

        // ', " are unchanged.

// }}}

// Bottom row {{{

        case 'z': return 'x';
        case 'Z': return 'X';

        case 'x': return 'c';
        case 'X': return 'C';

        case 'c': return 'd';
        case 'C': return 'D';

        case 'v': return 'v';
        case 'V': return 'V';

        case 'b': return 'z';
        case 'B': return 'Z';

        case 'n': return 'k';
        case 'N': return 'K';

        case 'm': return 'h';
        case 'M': return 'H';

        case ',': return ',';
        case '<': return '<';

        case '.': return '.';
        case '>': return '>';

        case '/': return '/';
        case '?': return '?';

// }}}

        default: return c;
    }
}

// }}}

// Tarmak 1 {{{

int convert_tarmak_1(int c)
{
    switch (c) {
        case 'E': return 'J';
        case 'e': return 'j';

        case 'J': return 'N';
        case 'j': return 'n';

        case 'K': return 'E';
        case 'k': return 'e';

        case 'N': return 'K';
        case 'n': return 'k';

        default:
            return c;
    }
}

// }}}

// Tarmak 2 {{{

int convert_tarmak_2(int c)
{
    switch (c) {
        case 'E': return 'F';
        case 'e': return 'f';

        case 'T': return 'G';
        case 't': return 'g';

        case 'F': return 'T';
        case 'f': return 't';

        case 'G': return 'J';
        case 'g': return 'j';

        case 'J': return 'N';
        case 'j': return 'n';

        case 'K': return 'E';
        case 'k': return 'e';

        case 'N': return 'K';
        case 'n': return 'k';

        default:
            return c;
    }
}

// }}}

// Tarmak 3 {{{

int convert_tarmak_3(int c)
{
    switch (c) {
        case 'E': return 'F';
        case 'e': return 'f';

        case 'R': return 'J';
        case 'r': return 'j';

        case 'T': return 'G';
        case 't': return 'g';

        case 'S': return 'R';
        case 's': return 'r';

        case 'D': return 'S';
        case 'd': return 's';

        case 'F': return 'T';
        case 'f': return 't';

        case 'G': return 'D';
        case 'g': return 'd';

        case 'J': return 'N';
        case 'j': return 'n';

        case 'K': return 'E';
        case 'k': return 'e';

        case 'N': return 'K';
        case 'n': return 'k';

        default:
            return c;
    }
}

// }}}

// Tarmak 4 {{{

int convert_tarmak_4(int c)
{
    switch (c) {
        case 'E': return 'F';
        case 'e': return 'f';

        case 'R': return 'P';
        case 'r': return 'p';

        case 'T': return 'G';
        case 't': return 'g';

        case 'Y': return 'J';
        case 'y': return 'j';

        case 'O': return 'Y';
        case 'o': return 'y';

        case 'P': return ':';
        case 'p': return ';';

        case 'S': return 'R';
        case 's': return 'r';

        case 'D': return 'S';
        case 'd': return 's';

        case 'F': return 'T';
        case 'f': return 't';

        case 'G': return 'D';
        case 'g': return 'd';

        case 'J': return 'N';
        case 'j': return 'n';

        case 'K': return 'E';
        case 'k': return 'e';

        case ':': return 'O';
        case ';': return 'o';

        case 'N': return 'K';
        case 'n': return 'k';

        default:
            return c;
    }
}

// }}}
