#include "keyboard.h"

void kb_free(keyboard* kb)
{
    if (kb->keys) free(kb->keys);
    if (kb->layout) free(kb->layout);
}

void k_draw(display* disp, key* k)
{
    int w = k->w;
    int h = k->h;
    int x = k->posx;
    int y = k->posy;
    if (k->is_hr) wattron(disp->win, COLOR_PAIR(3) | A_BOLD);

    mvwhline(disp->win, y, x, 0, w);
    mvwhline(disp->win, y+h, x, 0, w);
    mvwvline(disp->win, y, x, 0, h);
    mvwvline(disp->win, y, x+w, 0, h);

    mvwaddch(disp->win, y, x, ACS_ULCORNER);
    mvwaddch(disp->win, y, x+w, ACS_URCORNER);
    mvwaddch(disp->win, y+h, x, ACS_LLCORNER);
    mvwaddch(disp->win, y+h, x+w, ACS_LRCORNER);

    mvwprintw(disp->win, y+1, x+1, " %c  ", k->c);

    wattroff(disp->win, COLOR_PAIR(1) | COLOR_PAIR(2) | COLOR_PAIR(3) | COLOR_PAIR(4) | A_BOLD);
}

void k_draw_hl(display* disp, key* k)
{
    int w = k->w;
    int h = k->h;
    int x = k->posx;
    int y = k->posy;
    if (k->is_hr) wattron(disp->win, COLOR_PAIR(4) | A_BOLD);
    else wattron(disp->win, COLOR_PAIR(2) | A_BOLD);

    mvwhline(disp->win, y, x, 0, w);
    mvwhline(disp->win, y+h, x, 0, w);
    mvwvline(disp->win, y, x, 0, h);
    mvwvline(disp->win, y, x+w, 0, h);

    mvwaddch(disp->win, y, x, ACS_ULCORNER);
    mvwaddch(disp->win, y, x+w, ACS_URCORNER);
    mvwaddch(disp->win, y+h, x, ACS_LLCORNER);
    mvwaddch(disp->win, y+h, x+w, ACS_LRCORNER);

    mvwprintw(disp->win, y+1, x+1, " %c  ", k->c);

    wattroff(disp->win, COLOR_PAIR(1) | COLOR_PAIR(2) | COLOR_PAIR(3) | COLOR_PAIR(4) | A_BOLD);
}

void kb_clear_hl(keyboard* kb, display* disp)
{
    k_draw(disp, kb->last_hl);
    kb->last_hl = NULL;
    wrefresh(disp->win);
}

void kb_get_layout(keyboard* kb, int layout, int level)
{
    // Failsafe.
    if (layout < 0 || layout > 9) layout = 0;
    char* layouts[] = {"1234567890-=QWERTYUIOP[]ASDFGHJKL;'\\ZXCVBNM,./",  // #0 qwerty
                       "1234567890-=QWFPGJLUY;[]ARSTDHNEIO'\\ZXCVBKM,./",  // #1 colemak
                       "1234567890-=QWFPBJLUY;[]ARSTGMNEIO'\\XCDVZKH,./",  // #2 colemak-dh
                       "1234567890[]',.PYFGCRL/=AOEUIDHTNS-\\;QJKXBMWVZ",  // #3 dvorak
                       "{}/PFMLJ4321;QBYURSO.65=-KCDTHEAZ87\\'XGVWNI,09",  // #4 dvorak-l
                       "1234JLMFP/{}56Q.ORSUYB;=78ZAEHTDCK-\\90X,INWVG'",  // #5 dvorak-r
                       "1234567890-=QWJRTYUIOP[]ASDFGHNEL;'\\ZXCVBKM,./",  // #6 tarmak-1
                       "1234567890-=QWFRGYUIOP[]ASDTJHNEL;'\\ZXCVBKM,./",  // #7 tarmak-2
                       "1234567890-=QWFJGYUIOP[]ARSTDHNEL;'\\ZXCVBKM,./",  // #8 tarmak-3
                       "1234567890-=QWFPGJUIY;[]ARSTDHNELO'\\ZXCVBKM,./"}; // #9 tarmak-4

    int len = strlen(layouts[layout]);
    kb->nkeys = len;
    char* s = (char*)malloc(sizeof(char) * len + 1);
    if (!s) return;

    if (level >= 1 && level <= 5) {

        char* lvlkeys = getlayout(layout, level, 1);
        int keylen = strlen(lvlkeys);

        for (int i = 0; i < len; i++) {
            s[i] = ' ';
            for (int j = 0; j < keylen; j++) {
                if (lvlkeys[j] == layouts[layout][i]) {
                    s[i] = lvlkeys[j];
                    break;
                }
            }
        }

    } else {
        memcpy(s, layouts[layout], len);
    }

    s[len] = '\0';
    kb->layout = s;

    char* hr_keys[] = {"ASDFJKL;",  // #0 qwerty
                       "ARSTNEIO",  // #1 colemak
                       "ARSTNEIO",  // #2 colemak-dh
                       "AOEUHTNS",  // #3 dvorak
                       "CDTHEA",    // #4 dvorak-l
                       "AEHTDC",    // #5 dvorak-r
                       "ASDFNEL;",  // #6 Tarmak-1
                       "ASDTNEL;",  // #7 Tarmak-2
                       "ARSTNEL;",  // #8 Tarmak-3
                       "ARSTNELO"}; // #9 Tarmak-4
    len = strlen(hr_keys[layout]);
    char* hrk = (char*)malloc(sizeof(char) * len + 1);
    memcpy(hrk, hr_keys[layout], len);
    hrk[len] = '\0';
    kb->hrkeys = hrk;
}

int is_hrkey(keyboard* kb, char c)
{
    int len = strlen(kb->hrkeys);
    for (int i = 0; i < len; i++) {
        if (c == kb->hrkeys[i]) return 1;
    }
    return 0;
}

int kb_create(keyboard* kb)
{
    key* keys = (key*)malloc(sizeof(key) * kb->nkeys);
    if (!keys) return 0;

    static const int spec_layout = 34;

    int i = 0;
    int total_keys = kb->nkeys;
    int y = 0;
    int x = 0;
    int h = 2;
    int w = 5;

    int offset_1 = (total_keys > spec_layout) ? w * 0.5 : 0;
    int offset_2 = (total_keys > spec_layout) ? w * 0.9 : w * 0.5;
    int offset_3 = (total_keys > spec_layout) ? w * 1.3 : w * 0.9;
    kb->totwidth = (total_keys > spec_layout) ? w * 12 + kb->spacing * 12 + w * 0.9 : w * 12 + kb->spacing * 12 + w * 0.5;
    kb->totheight = (total_keys > spec_layout) ? h * 4 + kb->spacing * 4 : h * 3 + kb->spacing * 3;

    int mult = 0;

    int cap = 12;
    if (total_keys > spec_layout) {

        for (i = i; i < cap; i++) {
            keys[i].c = kb->layout[i];
            keys[i].posy = y;
            keys[i].posx = x;
            keys[i].h = 2;
            keys[i].w = 5;
            keys[i].is_hr = is_hrkey(kb, kb->layout[i]);
            x += (w + kb->spacing);
        }

        ++mult;
    }

    i = (total_keys > spec_layout) ? i : 0;
    x = offset_1;
    y = (total_keys > spec_layout) ? (h * mult + kb->spacing * mult) : 0;

    cap = (total_keys > spec_layout) ? 24 : 12;
    for (i = i; i < cap; i++) {
        keys[i].c = kb->layout[i];
        keys[i].posy = y;
        keys[i].posx = x;
        keys[i].h = 2;
        keys[i].w = 5;
        keys[i].is_hr = is_hrkey(kb, kb->layout[i]);
        x += (w + kb->spacing);

    }

    i = (total_keys > spec_layout) ? i : 12;
    x = offset_2;
    ++mult;
    y = (total_keys > spec_layout) ? (h * mult + kb->spacing * mult) : (h * mult + kb->spacing * mult);

    cap = (total_keys > spec_layout) ? 36 : 24;
    for (i = i; i < cap; i++) {
        keys[i].c = kb->layout[i];
        keys[i].posy = y;
        keys[i].posx = x;
        keys[i].h = 2;
        keys[i].w = 5;
        keys[i].is_hr = is_hrkey(kb, kb->layout[i]);
        x += (w + kb->spacing);

    }

    i = (total_keys > spec_layout) ? i : 24;
    x = offset_3;
    ++mult;
    y = (total_keys > spec_layout) ? (h * mult + kb->spacing * mult) : (h * mult + kb->spacing * mult);

    cap = (total_keys > spec_layout) ? 46 : 34;
    for (i = i; i < cap; i++) {
        keys[i].c = kb->layout[i];
        keys[i].posy = y;
        keys[i].posx = x;
        keys[i].h = 2;
        keys[i].w = 5;
        keys[i].is_hr = is_hrkey(kb, kb->layout[i]);
        x += (w + kb->spacing);
    }

    kb->keys = keys;

    return 1;
}

keyboard* kb_init(settings* s)
{
    keyboard* kb = (keyboard*)malloc(sizeof(keyboard));
    if (!kb) return NULL;
    kb->keys = NULL;

    kb->layout = 0;
    kb->nkeys = 0;
    kb->numrow = 0;
    kb->totwidth = 0;
    kb->totheight = 0;
    kb->spacing = 1;
    kb->last_hl = NULL;
    kb_get_layout(kb, s->layout, s->level);
    int res = kb_create(kb);
    if (!res) {
        free(kb);
        return NULL;
    }
    return kb;
}

void kb_highlight_key(keyboard* kb, display* disp, int c)
{
    if (c <= 122 && c >= 97) c -= 32;
    else if (c == ':') c = ';';
    else if (c == '?') c = '/';
    else if (c == '!') c = '1';

    int i;
    for (i = 0; i < kb->nkeys; i++) {
        if (kb->layout[i] == c) break;
    }

    if (kb->last_hl == &kb->keys[i]) return;

    if (kb->last_hl) {
        k_draw(disp, kb->last_hl);
    }

    k_draw_hl(disp, &kb->keys[i]);
    kb->last_hl = &kb->keys[i];

    wrefresh(disp->win);
}

void sneedville(keyboard* kb, display* disp)
{
    for (int i = 0; i < kb->nkeys; i++) {
        k_draw(disp, &kb->keys[i]);
    }
    wrefresh(disp->win);
}
