#include "display.h"
#include <curses.h>

display* display_init(int h, int w)
{
    display* d = (display*)malloc(sizeof(display));
    getmaxyx(stdscr, d->screenrows, d->screencols);
    d->win = NULL;
    d->posy = 0;
    d->posx = 0;
    d->winh = (h >= 3) ? h : 3;
    d->winw = w;
    d->offset = 1;
    return d;
}

void display_create(display* d)
{
    if (d->win) delwin(d->win);
    int y = (d->screenrows - d->winh) / 2;
    int x = (d->screencols - d->winw) / 2;
    d->posy = y;
    d->posx = x;
    WINDOW* new = newwin(d->winh, d->winw, y, x);
    keypad(new, 1);
    if (!new) return;
    d->win = new;
}

void display_recreate(display* d, int y, int x)
{
    if (d->win) delwin(d->win);
    d->winh = y;
    d->winw = x;
    int posy = (d->screenrows - d->winh) / 2;
    int posx = (d->screencols - d->winw) / 2;
    d->posy = posy;
    d->posx = posx;
    WINDOW* new = newwin(d->winh, d->winw, posy, posx);
    keypad(new, 1);
    if (!new) return;
    d->win = new;
}

display* display_init_create(int h, int w)
{
    display* disp = display_init(h, w);
    display_create(disp);
    return disp;
}

display* display_init_create_kb(int h, int w, int offset)
{
    display* disp = display_init(h, w);
    display_create(disp);
    mvwin(disp->win, disp->posy + offset, disp->posx);
    wclear(disp->win);
    wrefresh(disp->win);
    return disp;
}

void display_result(display* d, data* data, display* kbd, timer* t)
{
    wclear(d->win);
    wrefresh(d->win);
    if (kbd) {
        wclear(kbd->win);
        wrefresh(kbd->win);
    }
    int len;
    char str[64];
    if (data->kpresses) {
        double res = (data->kpresses - data->incor) / (double)data->kpresses;
        res *= 100;
        len = snprintf(str, sizeof(str), "Accuracy: %.2f%%", res);
    } else {
        len =  snprintf(str, sizeof(str), "No input given");
    }
    int offset = len / 2 - 1;
    display_recreate(d, 7, offset * 2 + len + 2);
    wmove(d->win, 1, offset+1);

    waddstr(d->win, str);
    timer_calc_time(t);
    char time[64];
    if (t->hours == 0) {
        if (t->minutes == 0) {
            snprintf(time, sizeof(time), "%ds", t->seconds);
        } else {
            snprintf(time, sizeof(time), "%dm %ds", t->minutes, t->seconds);
        }
    } else {
        snprintf(time, sizeof(time), "%dh %dm %ds", t->hours, t->minutes, t->seconds);
    }

    int timelen = strlen(time);
    int start = (d->winw - timelen) / 2;
    wmove(d->win, 3, start);
    waddstr(d->win, time);

    char wpm[64];
    double wpmres = data->wordcompl / ((double)t->time / 60);
    if (!data->wordcompl)
        snprintf(wpm, sizeof(wpm), "WPM: 0");
    else
        snprintf(wpm, sizeof(wpm), "WPM: %.1f", wpmres);

    int wpmlen = strlen(wpm);
    start = (d->winw - wpmlen) / 2;
    wmove(d->win, 5, start);

    waddstr(d->win, "WPM: ");
    if      (wpmres > 43) wattron(d->win, COLOR_PAIR(9) | A_BOLD); // 43-80 for average professional typist.
    else if (wpmres > 27) wattron(d->win, COLOR_PAIR(8) | A_BOLD); // 27-37 hunt and peck.
    else                  wattron(d->win, COLOR_PAIR(7) | A_BOLD); // Awful, lol.

    wprintw(d->win, "%.1f", wpmres);

    box(d->win, 0, 0);

    wrefresh(d->win);
    int c;
    while ((c = wgetch(d->win)) != ERR)
        if (c == 13) break;
    exit(0);
}

void display_free(display* d)
{
    delwin(d->win);
    d = NULL;
}
